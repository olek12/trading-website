import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.express as px
import plotly.graph_objects as go
import pandas as pd

import requests

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

markets = requests.get("https://api.bittrex.com/v3/markets").json()
market_names = [market["symbol"] for market in markets]
market_options = \
    [{"label": market_name, "value": market_name} for market_name in market_names]

graph_types = ["Line", "Bar", "Candlestick"]
graph_type_options = \
    [{"label": graph_type, "value": graph_type} for graph_type in graph_types]

df = pd.read_csv("BTC-USD.csv")

app.layout = html.Div([
    html.Div([
        dcc.Dropdown(
            id="market-name",
            options=market_options,
            value="BTC-USD",
            placeholder="Select market",
        ),
    ], style={"width": "20%", "display": "inline-block", "margin-right": "10px"}),
    html.Div([
        dcc.Dropdown(
            id="graph-type",
            options=graph_type_options,
            value="Line",
            placeholder="Select graph type",
        ),
    ], style={"width": "20%", "display": "inline-block"}),
    dcc.Graph(
        id="main-graph",
    ),
])


@app.callback(
    Output(component_id="main-graph", component_property="figure"),
    [Input(component_id="market-name", component_property="value"),
     Input(component_id="graph-type", component_property="value")]
)
def update(market_name, graph_type):
    if graph_type == "Line":
        return px.line(
            df,
            x="Date",
            y="Close",
            title=market_name
        )
    elif graph_type == "Bar":
        return go.Figure(
            go.Ohlc(
                x=df["Date"],
                open=df["Open"],
                high=df["High"],
                low=df["Low"],
                close=df["Close"],
            )
        )
    elif graph_type == "Candlestick":
        return go.Figure(
            go.Candlestick(
                x=df["Date"],
                open=df["Open"],
                high=df["High"],
                low=df["Low"],
                close=df["Close"],
            )
        )


if __name__ == '__main__':
    app.run_server(debug=True)
